<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\ProjectCategories as Categories;
use App\Models\Project\Project;
use App\Models\Project\ProjectTeam as Team;

use Storage;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $project = Project::orderBy('projects.categories_id','ASC')
                    ->join('project_categories','projects.categories_id','=','project_categories.categories_id');

        if($request->has('key') && $request->has('param')) {
            $project->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['project']   = $project->paginate(25);
        return view('project.index', $data);
    }
    
    public function create()
    {
        $data['category'] = Categories::orderBy('category','ASC')->get();

        return view('project.create', $data);
    }

    public function store(Request $request)
    {
        \DB::beginTransaction();
            $project = new Project;

            foreach($request->except('_token','project_quotation','team_name','team_position') as $key => $value)
            {
                if($key == "project_start" || $key == "project_end") { $project->{$key} = date('Y-m-d', strtotime($value)); }
                else { $project->{$key} = $value; }
            }

            //project quotation
            if($request->hasFile('project_quotation')) {
                $filename = $request->file('project_quotation')->hashName();

                $request->project_quotation->storeAs('public/project/', $filename);

                if ($request->file('project_quotation')->isValid()) {
                    $project->project_quotation = $filename;
                }
            }

            $project->save();

            $team_name      = $request->team_name;
            $team_position  = $request->team_position;

            for($i = 1 ; $i < sizeof($team_name); $i++)
            {
                $team = new Team;
                $team->team_name = $team_name[$i];
                $team->team_position = $team_position[$i];
                $team->project_id = $project->project_id;
                $team->save();
            }

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Project berhasil ditambahkan');
        \DB::commit();

        return redirect('project');
    }

    public function edit($id)
    {
        $project = Project::find($id);
        $team = Team::where('project_id',$id)->get();
        $category = Categories::orderBy('category','ASC')->get();

        return view('project.edit', compact('category','project','team'));
    }

    public function update(Request $request, $id)
    {
        \DB::beginTransaction();

            $project = Project::find($id);

            foreach($request->except('_token','_method','project_quotation','team_name','team_position') as $key => $value)
            {
                if($key == "project_start" || $key == "project_end") { $project->{$key} = date('Y-m-d', strtotime($value)); }
                else { $project->{$key} = $value; }
            }

            //project quotation
            if($request->hasFile('project_quotation')) {
                $filename = $request->file('project_quotation')->hashName();

                $oldFile = $project->project_quotation;

                //delete old files;
                Storage::delete('public/project/'.$oldFile);

                $request->project_quotation->storeAs('public/project/', $filename);

                if ($request->file('project_quotation')->isValid()) {
                    $project->project_quotation = $filename;
                }
            }

            $team_id        = $request->team_id;
            $team_name      = $request->team_name;
            $team_position  = $request->team_position;

            for($i = 1 ; $i < sizeof($team_name); $i++)
            {
                $team = isset($team_id[$i]) ? Team::find($team_id[$i]) : new Team;
                $team->team_name = $team_name[$i];
                $team->team_position = $team_position[$i];
                $team->project_id = $project->project_id;
                $team->save();
            }

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Project berhasil diubah');

        \DB::commit();

        return redirect('project');
    }

    public function show($id)
    {
        $project = Project::find($id);
        $team = Team::where('project_id',$id)->get();
        return view('project.show', compact('project','team'));
    }

    public function done(Request $request, $id)
    {
        $project                   = Project::find($id);
        $project->project_status   = $request->is_done;

        if($request->has('project_reason_terminate'))
            $project->project_reason_terminate = $request->project_reason_terminate;

        $status = "selesaikan";

        if($request->is_approve == 2)
            $status = "gagalkan";

        if($project->save()){            
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Project sudah di '. $status);
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Pengajuan gagal diproses');
        }

        return redirect()->back();
    }

    public function delete(Request $request, $id)
    {
        \DB::beginTransaction();
            $project = Project::find($id);
            $oldFile = $project->project_quotation;

            //delete old files;
            Storage::delete('public/project/'.$oldFile);

            $delete	= Project::findOrFail($id)->delete();

            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Project berhasil di hapus');
        \DB::commit();

    	return redirect()->back();
    }
}
