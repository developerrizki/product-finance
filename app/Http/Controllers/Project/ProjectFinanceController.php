<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Project\ProjectFinance as Finance;
use App\Models\Project\Project;
use App\Models\Project\ProjectTeam as Team;

class ProjectFinanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $finance = Finance::orderBy('project_finances.project_id','ASC')
                    ->join('projects','project_finances.project_id','=','projects.project_id');

        if($request->has('key') && $request->has('param')) {
            $finance->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['finance']   = $finance->paginate(25);
        
        return view('project.finance.index', $data);
    }
    
    public function create()
    {
        $data['project'] = Project::orderBy('project','ASC')->get();
        return view('project.finance.create', $data);
    }

    public function store(Request $request)
    {
        $finance = new Finance;

        foreach($request->except('_token') as $key => $value)
        {
            if($key == "pm_finance_date") { 
                if($value != "" || $value != NULL) {
                    $finance->{$key} = date('Y-m-d H:i:s', strtotime($value));
                }  
            }
            else { $finance->{$key} = $value; }
        }

        if($finance->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Keuangan Project berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Keuangan Project gagal ditambahkan');
        }

        return redirect('pfinance');
    }

    public function edit($id)
    {
        $finance  = Finance::find($id);
        $project  = Project::orderBy('project','ASC')->get();
        $team  = Team::orderBy('team_name','ASC')->get();

        return view('project.finance.edit', compact('project','finance','team'));
    }

    public function update(Request $request, $id)
    {
        $finance = Finance::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            if($key == "pm_finance_date") { 
                if($value != "" || $value != NULL) {
                    $finance->{$key} = date('Y-m-d H:i:s', strtotime($value));
                } 
            }
            else { $finance->{$key} = $value; }
        }

        if($finance->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Keuangan Project berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Keuangan Project gagal diubah');
        }

        return redirect('pfinance');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Finance::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Keuangan Project berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Keuangan Project gagal di hapus');
        }

    	return redirect()->back();
    }

    function getTeam($id)
    {
        $html = "<option value=''>Pilih Developer</option>";

        $team = Team::where('project_id', $id)->get();

        foreach ($team as $key => $value) {
            $html .= "<option value='".$value->team_id."'>". $value->team_name ." (". $value->team_position .")</option>";
        }

        return $html;
    }
}
