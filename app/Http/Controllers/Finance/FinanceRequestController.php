<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Finance\Finance;
use App\Models\Finance\FinanceRequest;
use App\Models\Master\FinanceCategories;

class FinanceRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user_id = \Auth::user()->id;
        $level_id = \Auth::user()->level_id;

        $frequest = FinanceRequest::orderBy('finance_requests.categories_id','ASC')
                    ->join('finance_categories','finance_requests.categories_id','=','finance_categories.categories_id');

        if($request->has('key') && $request->has('param')) {
            $frequest->where($request->param, 'like', '%'.$request->key.'%');
        }

        if($level_id != 1 && $level_id != 2)
            $frequest->where('user_id', $user_id);

        $data['frequest']   = $frequest->paginate(25);
        return view('finance.request.index', $data);
    }
    
    public function create()
    {
        $data['category'] = FinanceCategories::orderBy('category','ASC')->get();

        return view('finance.request.create', $data);
    }

    public function store(Request $request)
    {
        $frequest = new FinanceRequest;

        foreach($request->except('_token') as $key => $value)
        {
            if($key == "request_date") { $frequest->{$key} = date('Y-m-d H:i:s', strtotime($value)); }
            else { $frequest->{$key} = $value; }
        }

        $frequest->user_id = \Auth::user()->id;

        if($frequest->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Pengajuan berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Pengajuan gagal ditambahkan');
        }

        return redirect('frequest');
    }

    public function edit($id)
    {
        $frequest = FinanceRequest::find($id);
        $category = FinanceCategories::orderBy('category','ASC')->get();

        return view('finance.request.edit', compact('category','frequest'));
    }

    public function show($id)
    {
        $frequest = FinanceRequest::find($id);
        return view('finance.request.show', compact('frequest'));
    }

    public function approval(Request $request, $id)
    {
        $frequest                   = FinanceRequest::find($id);
        $frequest->approver_id      = \Auth::user()->id;
        $frequest->request_status   = $request->is_approve;

        if($request->has('request_reason_reject'))
            $frequest->request_reason_reject = $request->request_reason_reject;

        $status = "Diterima";

        if($request->is_approve == 2)
            $status = "Ditolak";

        if($frequest->save()){

            //if approval == 1
            if($request->is_approve == 1){
                $finance = new Finance;
                $finance->finance_info   = $frequest->request_title;
                $finance->finance_desc   = $frequest->request_description;
                $finance->finance_credit = $frequest->request_nominal;
                $finance->finance_pkey   = 0;
                $finance->finance_date   = date('Y-m-d H:i:s');
                $finance->category_id    = $frequest->categories_id;
                $finance->save();
            }
            
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Pengajuan berhasil '. $status);
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Pengajuan gagal '. $status);
        }

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $frequest = FinanceRequest::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            if($key == "request_date") { $frequest->{$key} = date('Y-m-d H:i:s', strtotime($value)); }
            else { $frequest->{$key} = $value; }
        }

        if($frequest->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Pengajuan berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Pengajuan gagal diubah');
        }

        return redirect('frequest');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= FinanceRequest::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Pengajuan berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Pengajuan gagal di hapus');
        }

    	return redirect()->back();
    }
}
