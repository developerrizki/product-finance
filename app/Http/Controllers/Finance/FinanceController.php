<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Finance\Finance;
use App\Models\Master\FinanceCategories;

class FinanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $finance = Finance::orderBy('category_id','ASC')
                    ->join('finance_categories','finances.category_id','=','finance_categories.categories_id');

        if($request->has('key') && $request->has('param')) {
            $finance->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['finance']   = $finance->paginate(25);
        return view('finance.index', $data);
    }
    
    public function create()
    {
        $data['category'] = FinanceCategories::orderBy('category','ASC')->get();

        return view('finance.create', $data);
    }

    public function store(Request $request)
    {
        $finance = new Finance;

        foreach($request->except('_token') as $key => $value)
        {
            if($key == "finance_date") { $finance->{$key} = date('Y-m-d H:i:s', strtotime($value)); }
            else { $finance->{$key} = $value; }
        }

        if($finance->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Keuangan berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Keuangan gagal ditambahkan');
        }

        return redirect('finance');
    }

    public function edit($id)
    {
        $finance = Finance::find($id);
        $category = FinanceCategories::orderBy('category','ASC')->get();

        return view('finance.edit', compact('category','finance'));
    }

    public function update(Request $request, $id)
    {
        $finance = Finance::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            if($key == "finance_date") { $finance->{$key} = date('Y-m-d H:i:s', strtotime($value)); }
            else { $finance->{$key} = $value; }
        }

        if($finance->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Keuangan berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Keuangan gagal diubah');
        }

        return redirect('finance');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Finance::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Keuangan berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Keuangan gagal di hapus');
        }

    	return redirect()->back();
    }
}
