<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\System\Module;

class ModuleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = Module::orderBy('module','ASC');

        if($request->has('key') && $request->has('param')) {
            $module->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['module']   = $module->paginate(25);
        return view('system.module.index', $data);
    }
    
    public function create()
    {
        $data['module'] = Module::orderBy('module','ASC')->get();
        return view('system.module.create', $data);
    }

    public function store(Request $request)
    {
        $module = new Module;

        foreach($request->except('_token') as $key => $value)
        {
            $module->{$key} = $value;
        }

        if($module->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Module berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Module gagal ditambahkan');
        }

        return redirect('module');
    }

    public function edit($id)
    {
        $module = Module::find($id);
        return view('system.module.edit', compact('module'));
    }

    public function update(Request $request, $id)
    {
        $module = Module::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            $module->{$key} = $value;
        }

        if($module->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Module berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Module gagal diubah');
        }

        return redirect('module');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Module::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Module berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Module gagal di hapus');
        }

    	return redirect('module');
    }
}
