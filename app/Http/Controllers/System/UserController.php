<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\System\Level;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $user = User::orderBy('name','ASC');

        if($request->has('key') && $request->has('param')) {
            $user->where($request->param, 'like', '%'.$request->key.'%');
        }

        if(\Auth::user()->level_id != 1)
            $user->whereNotIn('level_id', [1]);

        $data['user']   = $user->paginate(25);

        return view('system.user.index', $data);
    }
    
    public function create()
    {
        $level  = Level::orderBy('level','ASC')->get();
        
        if(\Auth::user()->level_id != 1)
            $level  = Level::orderBy('level','ASC')->whereNotIn('level_id', [1])->get();

        return view('system.user.create', compact('level'));
    }

    public function store(Request $request)
    {
        $user = new User;

        foreach($request->except('_token','password_confirmation') as $key => $value)
        {
            if($key == "password"){
                $user->{$key} = bcrypt($value);
            } else {
                $user->{$key} = $value;
            }
        }

        if($user->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'User gagal ditambahkan');
        }

        return redirect('user');
    }

    public function edit($id)
    {
        $user   = User::find($id);
        $level  = Level::orderBy('level','ASC')->get();

        return view('system.user.edit', compact('user','level'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        foreach($request->except(['_token','_method','password_confirmation']) as $key => $value)
        {
            if($value != "") {
                if($key == "password"){
                    $user->{$key} = bcrypt($value);
                } else {
                    $user->{$key} = $value;
                }
            }
                
        }

        if($user->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'User gagal diubah');
        }

    	return redirect('user');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= User::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'User berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'User gagal di hapus');
        }

    	return redirect()->back();
    }
}
