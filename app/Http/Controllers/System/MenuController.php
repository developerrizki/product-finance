<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\System\Menu;
use App\Models\System\Module;

class MenuController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu = Menu::orderBy('menu_id','ASC');

        if($request->has('key') && $request->has('param')) {
            $menu->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['menu']   = $menu->paginate(25);
        return view('system.menu.index', $data);
    }
    
    public function create()
    {
        $data['module'] = Module::orderBy('module','ASC')->get();
        $data['menu']   = Menu::orderBy('menu','ASC')->where('menu_is_sub',0)->get();

        return view('system.menu.create', $data);
    }

    public function store(Request $request)
    {
        $menu = new Menu;

        foreach($request->except('_token') as $key => $value)
        {
            if($value != "") {
                $menu->{$key} = $value;
            }
        }

        if($menu->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Menu berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Menu gagal ditambahkan');
        }

        return redirect('menu');
    }

    public function edit($id)
    {
        $menu     = Menu::find($id);
        $module   = Module::orderBy('module','ASC')->get();
        $parent   = Menu::orderBy('menu','ASC')->where('menu_is_sub',0)->get();

        return view('system.menu.edit', compact('module','menu','parent'));
    }

    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            if($value != "") {
                $menu->{$key} = $value;
            }
        }

        if($menu->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Menu berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Menu gagal diubah');
        }

        return redirect('menu');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Menu::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Menu berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Menu gagal di hapus');
        }

    	return redirect()->back();
    }
}
