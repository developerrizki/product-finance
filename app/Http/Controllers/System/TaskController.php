<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\System\Module;
use App\Models\System\Task;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $task = Task::orderBy('sys_tasks.module_id','ASC')
                    ->join('sys_modules', 'sys_tasks.module_id', '=', 'sys_modules.module_id');

        if($request->has('key') && $request->has('param')) {
            $task->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['task']   = $task->paginate(25);
        return view('system.task.index', $data);
    }
    
    public function create()
    {
        $data['module'] = Module::orderBy('module','ASC')->get();
        return view('system.task.create', $data);
    }

    public function store(Request $request)
    {
        foreach($request->task as $key)
        {
            $task            = new Task;
            $task->module_id = $request->module_id;
            $task->task      = $key;
            $task->save();
        }

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Task berhasil ditambahkan');

        return redirect('task');
    }

    public function edit($id)
    {
        $task = Task::find($id);
        $module = Module::orderBy('task','ASC')->get();

        return view('system.task.edit', compact('module','task'));
    }

    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            $task->{$key} = $value;
        }

        if($task->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Task berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Task gagal diubah');
        }

        return redirect()->back();
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Task::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Task berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Task gagal di hapus');
        }

    	return redirect()->back();
    }
}
