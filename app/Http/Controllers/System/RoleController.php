<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\System\Role;
use App\Models\System\Level;
use App\Models\System\Module;
use App\Models\System\Task;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $level = Level::orderBy('level','ASC');

        if($request->has('key') && $request->has('param')) {
            $level->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['level']   = $level->paginate(25);
        return view('system.role.index', $data);
    }

    public function create($id)
    {
        $level  = Level::find($id);
        $module = Module::orderBy('module_id', 'ASC')->get();

        return view('system.role.create', compact('module', 'level'));
    }

    public function store(Request $request)
    {
        //Delete All
        Role::where('level_id', $request->level_id)->delete();

        foreach ($request->task_id as $key) {
            $role           = new Role;
            $role->level_id = $request->level_id;
            $role->task_id  = $key;
            $role->save();
        }

        $request->session()->flash('status', '200');
        $request->session()->flash('msg', 'Role berhasil ditambahkan');

        return redirect('role');
    }
}
