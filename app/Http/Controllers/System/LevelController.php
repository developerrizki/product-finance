<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\System\Level;

class LevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $level = Level::orderBy('level','ASC');

        if($request->has('key') && $request->has('param')) {
            $level->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['level']   = $level->paginate(25);
        return view('system.level.index', $data);
    }
    
    public function create()
    {
        return view('system.level.create');
    }

    public function store(Request $request)
    {
        $level = new Level;

        foreach($request->except('_token') as $key => $value)
        {
            $level->{$key} = $value;
        }

        if($level->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Level berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('status', 'Level gagal ditambahkan');
        }

        return redirect('level');
    }

    public function edit($id)
    {
        $level = Level::find($id);

        return view('system.level.edit', compact('level'));
    }

    public function update(Request $request, $id)
    {
        $level = Level::find($id);

        foreach($request->except(['_token','_method']) as $key => $value)
        {
            $level->{$key} = $value;
        }

        if($level->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Level berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Level gagal diubah');
        }

    	return redirect('level');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= Level::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Level berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Level gagal di hapus');
        }

    	return redirect()->back();
    }
}
