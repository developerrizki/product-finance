<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\FinanceCategories;

class FinanceCategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $category = FinanceCategories::orderBy('category','ASC');

        if($request->has('key') && $request->has('param')) {
            $category->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['category']   = $category->paginate(25);
        return view('master.finance_category.index', $data);
    }
    
    public function create()
    {
        $data['category'] = FinanceCategories::orderBy('category','ASC')->get();
        return view('master.finance_category.create', $data);
    }

    public function store(Request $request)
    {
        $category = new FinanceCategories;

        foreach($request->except('_token') as $key => $value)
        {
            $category->{$key} = $value;
        }

        if($category->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori Keuangan berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kategori Keuangan gagal ditambahkan');
        }

        return redirect('fcategories');
    }

    public function edit($id)
    {
        $category = FinanceCategories::find($id);
        return view('master.finance_category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = FinanceCategories::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            $category->{$key} = $value;
        }

        if($category->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori Keuangan berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kategori Keuangan gagal diubah');
        }

        return redirect('fcategories');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= FinanceCategories::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori Keuangan berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kategori Keuangan gagal di hapus');
        }

    	return redirect()->back();
    }
}
