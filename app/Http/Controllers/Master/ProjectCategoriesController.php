<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Master\ProjectCategories;

class ProjectCategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $category = ProjectCategories::orderBy('category','ASC');

        if($request->has('key') && $request->has('param')) {
            $category->where($request->param, 'like', '%'.$request->key.'%');
        }

        $data['category']   = $category->paginate(25);
        return view('master.project_category.index', $data);
    }
    
    public function create()
    {
        $data['category'] = ProjectCategories::orderBy('category','ASC')->get();
        return view('master.project_category.create', $data);
    }

    public function store(Request $request)
    {
        $category = new ProjectCategories;

        foreach($request->except('_token') as $key => $value)
        {
            $category->{$key} = $value;
        }

        if($category->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori Project berhasil ditambahkan');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kategori Project gagal ditambahkan');
        }

        return redirect('pcategories');
    }

    public function edit($id)
    {
        $category = ProjectCategories::find($id);
        return view('master.project_category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = ProjectCategories::find($id);

        foreach($request->except('_token','_method') as $key => $value)
        {
            $category->{$key} = $value;
        }

        if($category->save()){
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori Project berhasil diubah');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kategori Project gagal diubah');
        }

        return redirect('pcategories');
    }

    public function delete(Request $request, $id)
    {
    	$delete	= ProjectCategories::findOrFail($id)->delete();

        if ($delete) {
            $request->session()->flash('status', '200');
            $request->session()->flash('msg', 'Kategori Project berhasil di hapus');
        } else {
            $request->session()->flash('status', 'err');
            $request->session()->flash('msg', 'Kategori Project gagal di hapus');
        }

    	return redirect()->back();
    }
}
