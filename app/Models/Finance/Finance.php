<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class Finance extends Model
{
    protected $table = "finances";
    protected $primaryKey = "finance_id";

    public function getCategory()
    {
        return $this->belongsTo('App\Models\Master\FinanceCategories','category_id','categories_id');
    }
}
