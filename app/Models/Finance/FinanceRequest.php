<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class FinanceRequest extends Model
{
    protected $table = "finance_requests";
    protected $primaryKey = "request_id";

    public function getCategory()
    {
        return $this->belongsTo('App\Models\Master\FinanceCategories','categories_id','categories_id');
    }

    public function getUser()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function getApprover()
    {
        return $this->belongsTo('App\User','approver_id','id');
    }
}
