<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "sys_roles";
    protected $primaryKey = "role_id";
}
