<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = "sys_tasks";
    protected $primaryKey = "task_id";

    public function getModule()
    {
        return $this->belongsTo('App\Models\System\Module','module_id','module_id');
    }
}
