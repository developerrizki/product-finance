<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "sys_menus";
    protected $primaryKey = "menu_id";

    public function getModule()
    {
        return $this->belongsTo('App\Models\System\Module','module_id','module_id');
    }

    public function getMenu()
    {
        return $this->belongsTo('App\Models\System\Menu','menu_parent','menu_id');
    }
}
