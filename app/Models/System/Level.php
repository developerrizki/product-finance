<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = "levels";
    protected $primaryKey = "level_id";
}
