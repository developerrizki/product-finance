<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectTeam extends Model
{
    protected $table = "project_teams";
    protected $primaryKey = "team_id";

    public function getCategory()
    {
        return $this->belongsTo('App\Models\Project\Project','project_id','project_id');
    }
}
