<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectFinance extends Model
{
    protected $table = "project_finances";
    protected $primaryKey = "pm_finance_id";

    public function getTeam()
    {
        return $this->belongsTo('App\Models\Project\ProjectTeam','team_id','team_id');
    }

    public function getProject()
    {
        return $this->belongsTo('App\Models\Project\Project','project_id','project_id');
    }
}
