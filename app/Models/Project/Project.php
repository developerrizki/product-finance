<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = "projects";
    protected $primaryKey = "project_id";

    public function getCategory()
    {
        return $this->belongsTo('App\Models\Master\ProjectCategories','categories_id','categories_id');
    }

    public function getUser()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
