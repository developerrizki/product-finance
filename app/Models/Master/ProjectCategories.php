<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProjectCategories extends Model
{
    protected $table = "project_categories";
    protected $primaryKey = "categories_id";
}
