<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class FinanceCategories extends Model
{
    protected $table = "finance_categories";
    protected $primaryKey = "categories_id";
}
