@extends('layouts.root')

@section('title', 'Tambah Kategori Keuangan')
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Kategori Keuangan</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Master</a></li>
                    <li><a href="{{ url('fcategories') }}">Kategori Keuangan</a></li>
                    <li class="active">Tambah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" data-toggle="validator" method="POST" action="{{ url('fcategories') }}">
                        @csrf
                        <div class="col-8">
                            <div class="form-group">
                                <label>Kategori</label>
                                <input type="text" class="form-control" name="category" placeholder="Masukan kategori" required data-error="Kategori harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label>Persentase (%)</label>
                                <input type="text" class="form-control" name="category_percentage" value="0" placeholder="Masukan persentase" required data-error="Persentase harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('fcategories') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/validator.js') }}"></script>
@endpush