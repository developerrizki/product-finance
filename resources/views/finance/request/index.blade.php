@extends('layouts.root')

@section('title', 'List Pengajuan Keuangan')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Pengajuan Keuangan</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Finance</a></li>
                    <li class="active">Pengajuan Keuangan</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">
                
                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <a href="{{ url('frequest/create') }}" class="btn btn-primary">
                            <i class="ti-plus"></i> &nbsp; Tambah Pengajuan</i>
                        </a>
                        <div class="pull-right">
                            <form class="form-inline float-right" action="{{ url('frequest') }}">
                                <div class="form-group" style="margin-right: 10px;">
                                    <select name="param" id="param" class="form-control">
                                        <option value="">-- Cari Berdasarkan --</option>
                                        <option value="category" {{ Request::get('param') == "category" ? "selected" : "" }} >Kategori</option>
                                    </select>
                                </div>
                                <div class="form-group" style="margin-right: 10px;">
                                    <input type="text" name="key" id="key" class="form-control" value='{{ Request::has("key") ? Request::get("key") : "" }}' placeholder="Masukan pencarian">
                                </div>
                                <button style="margin-right: 10px;" type="submit" class="btn btn-success"><i class="icon-search"></i> Cari</button>
                                <a href="{{ url('frequest') }}" class="btn btn-default btn-outline" style="text-decoration:none"><i class="ti-reload"></i></a>
                            </form>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Judul</th>
                                            <th>Deskripsi</th>
                                            <th>Kategori</th>
                                            <th>Tanggal</th>
                                            <th>Nominal</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($frequest) > 0)
                                            @foreach ($frequest as $key => $result)
                                                @php $status = "";

                                                    if ($result->request_status == 0) {
                                                        $status = "<span class='btn btn-warning'>Pending</span>";
                                                    }
                                                    elseif($result->request_status == 1) {
                                                        $status = "<span class='btn btn-success'>Disetujui</span>";
                                                    }
                                                    else {
                                                        $status = "<span class='btn btn-danger'>Ditolak</span>";
                                                    }

                                                @endphp

                                                <tr>
                                                    <td>{{ ($frequest->currentpage()-1) * $frequest->perpage() + $key + 1  }}</td>
                                                    <td>{{ $result->request_title }}</td>
                                                    <td>{{ $result->request_description }}</td>
                                                    <td>{{ $result->getCategory->category }}</td>
                                                    <td width="12%">{{ date('d M Y', strtotime($result->request_date)) }}</td>
                                                    <td width="12%">Rp. {{ number_format($result->request_nominal) }}</td>
                                                    <td>{!! $status !!}</td>
                                                    <td width="15%">
                                                        <a href="{{ url('frequest/detail/'. $result->request_id) }}" class="btn btn-default btn-outline">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                        <a href="{{ url('frequest/edit/'. $result->request_id) }}" class="btn btn-info">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <button data-toggle="tooltip" title="Hapus" data-action="{{ url('frequest/delete/'. $result->request_id) }}" class="btn btn-danger btn-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr> <td colspan="7">Data Pengajuan Keuangan Tidak Tersedia. </td> </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $frequest->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- /.container-fluid -->

@include('modal_delete');
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Hapus Pengajuan Keuangan');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
