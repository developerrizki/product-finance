@extends('layouts.root')

@section('title', 'Detail Pengajuan Keuangan')

@push('style')    
    <link href="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@php $status = "";

    if ($frequest->request_status == 0) {
        $status = "<span class='btn-sm btn-warning'>Pending</span>";
    }
    elseif($frequest->request_status == 1) {
        $status = "<span class='btn-sm btn-success'>Disetujui</span>";
    }
    else {
        $status = "<span class='btn-sm btn-danger'>Ditolak</span>";
    }

@endphp

    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Pengajuan Keuangan</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Finance</a></li>
                    <li><a href="{{ url('frequest') }}">Pengajuan Keuangan</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">
                
                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        Tanggal Pengajuan : {{ date('d F Y', strtotime($frequest->request_date)) }}
                        @if($frequest->request_status == 0)
                            <div class="pull-right">
                                <a href="{{ url('frequest/approval/'.$frequest->request_id.'?is_approve=1') }}" class="btn btn-success" style="text-decoration:none">Setuju</a>
                                <button data-action="{{ url('frequest/approval/'.$frequest->request_id) }}" class="btn btn-danger btn-reject">TOLAK</button>
                            </div>
                        @endif
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-6">
                                    <label for="">Judul Pengajuan</label> <br>
                                    {{ $frequest->request_title }}
                                </div>
                                <div class="col-6">
                                    <label for="">Kategori</label> <br>
                                    {{ $frequest->getCategory->category }}
                                </div>
                                <div class="col-12 m-t-30">
                                    <label for="">Deskripsi Pengajuan</label> <br>
                                    {{ $frequest->request_description }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">Nominal Pengajuan</label> <br>
                                    Rp. {{ number_format($frequest->request_nominal) }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">Status Pengajuan</label> <br>
                                    {!! $status !!}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">User Pengaju</label> <br>
                                    {{ $frequest->getUser->name }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">User Approval</label> <br>
                                    {{ isset($frequest->getApprover) ? $frequest->getApprover->name : "-" }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">Alasan Ditolak</label> <br>
                                    {{ isset($frequest->request_reason_reject) ? $frequest->request_reason_reject : "-" }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
@include('modal_reject')
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        });


        $('.btn-reject').click(function(){
            var action = $(this).data('action');

            $('#modalReject').modal('show');
            $('.modal-title').html('Tolak Pengajuan Keuangan');

            $('#formReject').attr('action',action);
        });
    </script>
    <script src="{{ asset('js/validator.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('eliteadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@endpush