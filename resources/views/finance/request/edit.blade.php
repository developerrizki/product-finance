@extends('layouts.root')

@section('title', 'Ubah Pengajuan Keuangan')
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Pengajuan Keuangan</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Finance</a></li>
                    <li><a href="{{ url('frequest') }}">Pengajuan Keuangan</a></li>
                    <li class="active">Ubah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" method="POST" action="{{ url('frequest/update/'.$frequest->request_id) }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-6">
                                <div class="form-group">
                                    <label>Judul <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="request_title" placeholder="Masukan judul pengajuan" value="{{ $frequest->request_title }}" required data-error="Judul harus diisi">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Tanggal <span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="datepicker-autoclose" name="request_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($frequest->request_date)) }}" required data-error="Tanggal harus dipilih">
                                        <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                    </div>
    
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Deskripsi</label>
                                    <textarea name="request_description" id="request_description" cols="30" rows="5" class="form-control">{{ $frequest->request_description }}</textarea>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Kategori <span class="text-danger">*</span></label>
                                    <select name="categories_id" id="categories_id" class="selectpicker form-control" data-style="form-control" required data-error="Kategori harus dipilih">
                                        <option value="">Pilih Kategori</option>
                                        @foreach ($category as $result)
                                            <option value="{{ $result->categories_id }}" {{ $frequest->categories_id == $result->categories_id ? "selected" : "" }}>{{ $result->category }}</option>
                                        @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label>Nominal</label>
                                    <input type="text" class="form-control" name="request_nominal" value="{{ $frequest->request_nominal }}" placeholder="Masukan nominal debit" required data-error="Keterangan harus diisi">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('frequest') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection