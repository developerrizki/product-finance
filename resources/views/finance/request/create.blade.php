@extends('layouts.root')

@section('title', 'Tambah Pengajuan Keuangan')

@push('style')    
    <link href="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Pengajuan Keuangan</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Finance</a></li>
                    <li><a href="{{ url('frequest') }}">Pengajuan Keuangan</a></li>
                    <li class="active">Tambah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" data-toggle="validator" method="POST" action="{{ url('frequest') }}">
                        @csrf
                        <div class="col-6">
                            <div class="form-group">
                                <label>Judul <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="request_title" placeholder="Masukan judul pengajuan" required data-error="Judul harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tanggal <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="datepicker-autoclose" name="request_date" placeholder="mm/dd/yyyy" required data-error="Tanggal harus dipilih">
                                    <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                </div>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="request_description" id="request_description" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Kategori <span class="text-danger">*</span></label>
                                <select name="categories_id" id="categories_id" class="selectpicker form-control" data-style="form-control" required data-error="Kategori harus dipilih">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($category as $result)
                                        <option value="{{ $result->categories_id }}">{{ $result->category }}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Nominal</label>
                                <input type="text" value="0" class="form-control" name="request_nominal" placeholder="Masukan nominal" required data-error="Keterangan harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('finance') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        });
    </script>
    <script src="{{ asset('js/validator.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('eliteadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@endpush