<ul class="nav" id="side-menu">
    @php 
        $user_id = \Auth::user()->level_id;

        $role    = App\Models\System\Role::where('level_id', $user_id)
                    ->join('sys_tasks','sys_roles.task_id','sys_tasks.task_id')
                    ->join('sys_modules','sys_tasks.module_id','sys_modules.module_id')
                    ->groupBy('sys_modules.module_id')
                    ->pluck('sys_modules.module_id')->toArray();

        $menu = App\Models\System\Menu::where('menu_is_sub',0)->orderBy('menu_position','ASC')->get();
    @endphp
    @foreach ($menu as $result)
        @php $check_sub = App\Models\System\Menu::where('menu_parent',$result->menu_id)->count(); @endphp
        @if ($check_sub == 0)
            <li><a href="{{ url($result->menu_url) }}" class="waves-effect"><i class="{{ $result->menu_icon }} linea-icon linea-basic fa-fw"></i> <span class="hide-menu">{{ $result->menu }}</span></a> </li>
        @else
            @php 
                $available_sub = App\Models\System\Menu::where('menu_parent', $result->menu_id )  
                    ->whereIn('module_id', $role)
                    ->orderBy('menu_position','ASC')->count(); 
            @endphp
            @if ($available_sub != 0)
                <li>
                    <a href="#" class="waves-effect"><i class="{{ $result->menu_icon }} linea-icon linea-basic fa-fw"></i> <span class="hide-menu">{{ $result->menu }}<span class="fa arrow"></span></span></a>
                    <ul class="nav nav-second-level">
                        @php 
                            $sub = App\Models\System\Menu::where('menu_parent', $result->menu_id )  
                                ->whereIn('module_id', $role)
                                ->orderBy('menu_position','ASC')->get(); 
                        @endphp
                        @foreach ($sub as $item)
                            <li><a href="{{ url($item->menu_url) }}">{{ $item->menu }}</a></li>
                        @endforeach
                    </ul>
                </li>
            @endif
        @endif
    @endforeach
</ul>