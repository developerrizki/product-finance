@extends('layouts.root')

@section('title', 'List Level')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">FINANCE SAMPULKREATIV</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li class="active">Level</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">
                
                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <a href="{{ url('level/create') }}" class="btn btn-primary">
                            <i class="ti-plus"></i> &nbsp; Tambah Level</i>
                        </a>
                        <div class="pull-right">
                            <form class="form-inline float-right" action="{{ url('level') }}">
                                <div class="form-group" style="margin-right: 10px;">
                                    <select name="param" id="param" class="form-control">
                                        <option value="">-- Cari Berdasarkan --</option>
                                        <option value="level" {{ Request::get('param') == "level" ? "selected" : "" }} >Level</option>
                                    </select>
                                </div>
                                <div class="form-group" style="margin-right: 10px;">
                                    <input type="text" name="key" id="key" class="form-control" value='{{ Request::has("key") ? Request::get("key") : "" }}' placeholder="Masukan pencarian">
                                </div>
                                <button style="margin-right: 10px;" type="submit" class="btn btn-success"><i class="icon-search"></i> Cari</button>
                                <a href="{{ url('level') }}" class="btn btn-default" style="text-decoration:none"><i class="ti-reload"></i></a>
                            </form>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th width="75%">Level</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($level) > 0)
                                            @foreach ($level as $key => $result)
                                                <tr>
                                                    <td>{{ ($level->currentpage()-1) * $level->perpage() + $key + 1  }}</td>
                                                    <td>{{ $result->level }}</td>
                                                    <td>
                                                        <a href="{{ url('level/edit/'. $result->level_id) }}" class="btn btn-info">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <button data-toggle="tooltip" title="Hapus" data-action="{{ url('level/delete/'. $result->level_id) }}" class="btn btn-danger btn-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr> <td colspan="3">Data Level Tidak Tersedia. </td> </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $level->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- /.container-fluid -->

@include('modal_delete');
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Hapus Level');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
