@extends('layouts.root')

@section('title', 'Ubah Module')
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Module</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li><a href="{{ url('module') }}">Module</a></li>
                    <li class="active">Ubah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" method="POST" action="{{ url('module/update/'.$module->module_id) }}" data-toggle="validator">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Module</label>
                                <input type="text" class="form-control" name="module" placeholder="Masukan nama module" value="{{ $module->module }}" data-error="Module harus diisi" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('module') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/validator.js') }}"></script>
@endpush