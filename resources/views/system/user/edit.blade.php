@extends('layouts.root')

@section('title', 'Ubah User')

@push('style')    
    <link href="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.css') }}  " rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}    " rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }} " rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/multiselect/css/multi-select.css') }} " rel="stylesheet" type="text/css" />
@endpush
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">System User</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li><a href="{{ url('user') }}">User</a></li>
                    <li class="active">Tambah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" method="POST" action="{{ url('user/update/'.$user->id) }}" data-toggle="validator">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Level <span class="text-danger">*</span></label>
                                <select name="level_id" id="level_id" class="form-control select2" data-error="Level harus diisi" required>
                                    <option value="">Pilih Level</option>
                                    @foreach ($level as $result)
                                        <option value="{{ $result->level_id }}" {{ $user->level_id == $result->level_id ? "selected" : "" }}>{{ $result->level }}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nama <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="name" placeholder="Masukan nama" data-error="Nama harus diisi." required value="{{ $user->name }}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Username <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="username" placeholder="Masukan username" data-error="Username harus diisi." required value="{{ $user->username }}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Email <span class="text-danger">*</span></label>
                                <input type="email" class="form-control" name="email" placeholder="Masukan email" data-error="Email harus diisi." required value="{{ $user->email }}">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Kata Sandi <span class="text-danger">(isi jika ingin mengubah kata sandi)</span></label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Masukan kata sandi" data-error="Kata sandi harus diisi." data-minlength="6">
                                <span class="help-block">Minimal 6 karakter</span>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Ulangi Kata Sandi</label>
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Ulangi kata sandi" data-match="#password" data-match-error="Kata sandi tidak cocok"">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('user') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
        });
    </script>
    <script src="{{ asset('js/validator.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('eliteadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
@endpush