@extends('layouts.root')

@section('title', 'Tambah Role')
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Role</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li><a href="{{ url('role') }}">Role</a></li>
                    <li class="active">Tambah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        {{ $level->level }}
                        <div class="pull-right">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" id="taskAll" class="form-check-input" value="all"> Pilih Semua
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <form class="form-horizontal row" method="POST" action="{{ url('role') }}">
                                @csrf
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width="5%">No</th>
                                                <th width="25%">Module</th>
                                                <th>List</th>
                                                <th>Add</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                                <th>View</th>
                                                <th>Print</th>
                                                <th>Profile</th>
                                                <th>Setting</th>
                                                <th>Logout</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($module as $key => $result)
                                                @php 
                                                    $task = App\Models\System\Task::where('module_id', $result->module_id)->pluck('task','task_id')->toArray(); 
                                                    $role = App\Models\System\Role::where('level_id', $level->level_id)->pluck('task_id')->toArray();
                                                @endphp
                                                <tr>
                                                    <td>{{ $key + 1 }}</td>
                                                    <td>{{ $result->module }}</td>
                                                    <td>
                                                        @if (in_array("list", $task))
                                                            @if(in_array(array_search('list',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('list', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('list', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("add", $task))
                                                            @if(in_array(array_search('add',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('add', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('add', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("edit", $task))
                                                            @if(in_array(array_search('edit',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('edit', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('edit', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("delete", $task))
                                                            @if(in_array(array_search('delete',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('delete', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('delete', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("view", $task))
                                                            @if(in_array(array_search('view',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('view', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('view', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("print", $task))
                                                            @if(in_array(array_search('print',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('print', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('print', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("profile", $task))
                                                            @if(in_array(array_search('profile',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('profile', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('profile', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("setting", $task))
                                                            @if(in_array(array_search('setting',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('setting', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('setting', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if (in_array("logout", $task))
                                                            @if(in_array(array_search('logout',$task), $role))
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('logout', $task) }}" checked>
                                                            @else
                                                                <input type="checkbox" name="task_id[]" value="{{ array_search('logout', $task) }}">
                                                            @endif
                                                        @else
                                                            -
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="11">
                                                    <button class="btn btn-primary">Simpan</button>
                                                    <a href="{{ url('role') }}" class="btn btn-danger">Batal</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" name="level_id" value="{{ $level->level_id }}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $("#taskAll").click(function(){
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
        });
    </script>
@endpush