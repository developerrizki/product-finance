@extends('layouts.root')

@section('title', 'List Menu')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">System Menu</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li class="active">Menu</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">
                
                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <a href="{{ url('menu/create') }}" class="btn btn-primary">
                            <i class="ti-plus"></i> &nbsp; Tambah Menu</i>
                        </a>
                        <div class="pull-right">
                            <form class="form-inline float-right" action="{{ url('menu') }}">
                                <div class="form-group" style="margin-right: 10px;">
                                    <select name="param" id="param" class="form-control">
                                        <option value="">-- Cari Berdasarkan --</option>
                                        <option value="menu">Menu</option>
                                        <option value="menu_url">URL</option>
                                    </select>
                                </div>
                                <div class="form-group" style="margin-right: 10px;">
                                    <input type="text" name="key" id="key" class="form-control" value="" placeholder="Masukan pencarian">
                                </div>
                                <button style="margin-right: 10px;" type="submit" class="btn btn-success"><i class="icon-search"></i> Cari</button>
                                <a href="{{ url('menu') }}" class="btn btn-default" style="text-decoration:none"><i class="ti-reload"></i></a>
                            </form>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Menu</th>
                                            <th>Module</th>
                                            <th>URL</th>
                                            <th>Icon</th>
                                            <th>Sub</th>
                                            <th>Parent</th>
                                            <th>Position</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($menu) > 0)
                                            @foreach ($menu as $key => $result)
                                                <tr>
                                                    <td>{{ ($menu->currentpage()-1) * $menu->perpage() + $key + 1  }}</td>
                                                    <td>{{ $result->menu }}</td>
                                                    <td>{{ isset($result->getModule) ? $result->getModule->module : "-" }}</td>
                                                    <td>{{ $result->menu_url }}</td>
                                                    <td>{{ isset($result->menu_icon) ? $result->menu_icon : "-" }}</td>
                                                    <td>{{ $result->menu_is_sub == 0 ? "Bukan" : "Ya" }}</td>
                                                    <td>{{ isset($result->menu_parent) ? $result->getMenu->menu : "-" }}</td>
                                                    <td>{{ $result->menu_position }}</td>
                                                    <td>
                                                        <a href="{{ url('menu/edit/'. $result->menu_id) }}" class="btn btn-info">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <button data-toggle="tooltip" title="Hapus" data-action="{{ url('menu/delete/'. $result->menu_id) }}" class="btn btn-danger btn-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr> <td colspan="9">Data Menu Tidak Tersedia. </td> </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- /.container-fluid -->

@include('modal_delete');
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Hapus Menu');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
