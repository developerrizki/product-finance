@extends('layouts.root')

@section('title', 'Ubah Menu')
    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Menu</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li><a href="{{ url('menu') }}">Menu</a></li>
                    <li class="active">Ubah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" method="POST" action="{{ url('menu/update/'.$menu->menu_id) }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Menu</label>
                                <input type="text" class="form-control"  id="menu" name="menu" value="{{ $menu->menu }}" placeholder="Masukan nama menu">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Module</label>
                                <select class="form-control" id="module_id" name="module_id">
                                    <option value="">Pilih Module</option>
                                    @foreach ($module as $result)
                                        <option value="{{ $result->module_id }}" {{ $menu->module_id == $result->module_id ? "selected" : "" }}>{{ $result->module }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>URL</label>
                                <input type="text" class="form-control" id="menu_url" name="menu_url" value="{{ $menu->menu_url }}" placeholder="Masukan url">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Icon</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Masukan kode icon" id="menu_icon" name="menu_icon" value="{{ $menu->menu_icon }}">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-toggle="modal" data-target=".modal-icon">Lihat Icon</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Is Sub</label>
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="menu_is_sub" value="1" {{ $menu->menu_is_sub == 1 ? "checked=checked" : "" }}>
                                            <label>Ya</label>
                                        </div>
                                    </label>
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="menu_is_sub" value="0" {{ $menu->menu_is_sub == 0 ? "checked=checked" : "" }}>
                                            <label>Bukan</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Parent</label>
                                <select class="form-control" disabled id="menu_parent" name="menu_parent">
                                    <option value="">Pilih Menu</option>
                                    @foreach ($parent as $result)
                                        <option value="{{ $result->menu_id }}" {{ $menu->menu_parent == $result->menu_id ? "selected" : "" }}>{{ $result->menu }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Posisi</label>
                                <input type="text" class="form-control" id="menu_position" name="menu_position" placeholder="Masukan posisi. note : 0 - n" value="{{ $menu->menu_position }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('menu') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@include('modal_icon');
@endsection

@push('script')
    <script>
        $(document).ready(function(){

            var is_sub = "{{ $menu->menu_is_sub }}";

            if(is_sub == 1){
                $('#menu_parent').attr('disabled',false);
            }
            else {
                $('#menu_parent').attr('disabled',true);
            }

            $('input[name="menu_is_sub"]').bind('change', function(){
                if($(this).val() == 1){
                    $('#menu_parent').attr('disabled',false);
                }
                else {
                    $('#menu_parent').attr('disabled',true);
                }
            });
        });
    </script>
@endpush