@extends('layouts.root')

@section('title', 'List Task')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Task</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">System</a></li>
                    <li class="active">Task</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif
                
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <a href="{{ url('task/create') }}" class="btn btn-primary">
                            <i class="ti-plus"></i> &nbsp; Tambah task</i>
                        </a>
                        <div class="pull-right">
                            <form class="form-inline float-right" action="{{ url('task') }}">
                                <div class="form-group" style="margin-right: 10px;">
                                    <select name="param" id="param" class="form-control">
                                        <option value="">-- Cari Berdasarkan --</option>
                                        <option value="module" {{ Request::get('param') == "module" ? "selected" : "" }}>Module</option>
                                        <option value="task" {{ Request::get('param') == "task" ? "selected" : "" }}>Task</option>
                                    </select>
                                </div>
                                <div class="form-group" style="margin-right: 10px;">
                                    <input type="text" name="key" id="key" class="form-control" placeholder="Masukan pencarian" value="{{ Request::has('key') ? Request::get('key') : '' }}">
                                </div>
                                <button style="margin-right: 10px;" type="submit" class="btn btn-success"><i class="icon-search"></i> Cari</button>
                                <a href="{{ url('task') }}" class="btn btn-default" style="text-decoration:none"><i class="ti-reload"></i></a>
                            </form>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th width="35%">Module</th>
                                            <th width="35%">Task</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($task) > 0)
                                            @foreach ($task as $key => $result)
                                                <tr>
                                                    <td>{{ ($task->currentpage()-1) * $task->perpage() + $key + 1  }}</td>
                                                    <td>{{ $result->getModule->module }}</td>
                                                    <td>{{ $result->task }}</td>
                                                    <td>
                                                        {{--  <a href="{{ url('task/edit/'. $result->task_id) }}" class="btn btn-info">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>  --}}
                                                        <button data-toggle="tooltip" title="Hapus" data-action="{{ url('task/delete/'. $result->task_id) }}" class="btn btn-danger btn-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr> <td colspan="4">Data Task Tidak Tersedia. </td> </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $task->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- /.container-fluid -->

@include('modal_delete');
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Hapus Task');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
