@extends('layouts.root')

@section('title', 'Detail Project')

@push('style')    
    <link href="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@php $status = "";

    if ($project->project_status == 0) {
        $status = "<span class='btn-sm btn-warning'>Pengerjaan</span>";
    }
    elseif($project->project_status == 1) {
        $status = "<span class='btn-sm btn-success'>Selesai</span>";
    }
    else {
        $status = "<span class='btn-sm btn-danger'>Gagal</span>";
    }

@endphp

    
@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Project</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="{{ url('project') }}">Project</a></li>
                    <li class="active">Detail</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">
                
                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        Tanggal Project : {{ date('d F Y', strtotime($project->project_start)) }} s/d {{ date('d F Y', strtotime($project->project_end)) }}
                        @if($project->project_status == 0)
                            <div class="pull-right">
                                <a href="{{ url('project/done/'.$project->project_id.'?is_done=1') }}" class="btn btn-success" style="text-decoration:none">Selesai</a>
                                <button data-action="{{ url('project/done/'.$project->project_id) }}" class="btn btn-danger btn-terminate">GAGAL</button>
                            </div>
                        @endif
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-6">
                                    <label for="">Nama Project</label> <br>
                                    {{ $project->project }}
                                </div>
                                <div class="col-6">
                                    <label for="">Kategori</label> <br>
                                    {{ $project->getCategory->category }}
                                </div>
                                <div class="col-12 m-t-30">
                                    <label for="">Sumber Project</label> <br>
                                    {{ $project->project_source }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">Price</label> <br>
                                    Rp. {{ number_format($project->project_price) }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">Status Project</label> <br>
                                    {!! $status !!}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">Project Manager</label> <br>
                                    {{ $project->getUser->name }}
                                </div>
                                <div class="col-6 m-t-30">
                                    <label for="">File Penawaran</label> <br>
                                    <a href="{{ asset('storage/project/'. $project->project_quotation) }}" target="_blank">Lihat penawaran project {{ $project->project }}</a>
                                </div>
                            </div>

                            <div class="row m-t-30">
                                <div class="col-12">
                                    <h3>Tim Project</h3>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="5%">No</th>
                                                    <th>Nama</th>
                                                    <th>Posisi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(sizeof($team) > 0)
                                                    @foreach ($team as $key => $result)
                                                        <tr>
                                                            <td>{{ $key + 1  }}</td>
                                                            <td>{{ $result->team_name }}</td>
                                                            <td>{{ $result->team_position }}</td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr> <td colspan="3">Data Tim Tidak Tersedia. </td> </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
@include('modal_terminate')
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        });

        $('.btn-terminate').click(function(){
            var action = $(this).data('action');

            $('#modalTerminate').modal('show');
            $('.modal-title').html('Tolak Project');

            $('#formTerminate').attr('action',action);
        });
    </script>
    <script src="{{ asset('js/validator.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('eliteadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@endpush