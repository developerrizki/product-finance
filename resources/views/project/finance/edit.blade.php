@extends('layouts.root')

@section('title', 'Ubah Keuangan Project')
    
@push('style')    
    <link href="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Keuangan Project</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Project</a></li>
                    <li><a href="{{ url('pfinance') }}">Keuangan Project</a></li>
                    <li class="active">Ubah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form class="form-horizontal row" method="POST" action="{{ url('pfinance/update/'.$finance->pm_finance_id) }}">
                        @csrf
                        <input type="hidden" name="_method" value="PUT">
                        <div class="col-6">
                            <div class="form-group">
                                <label>Keterangan <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="pm_finance_info" placeholder="Masukan keterangan keuangan Project" value="{{ $finance->pm_finance_info }}" required data-error="Keterangan harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Tanggal <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="datepicker-autoclose" name="pm_finance_date" placeholder="mm/dd/yyyy" value="{{ date('m/d/Y', strtotime($finance->pm_finance_date)) }}" required data-error="Tanggal harus dipilih">
                                    <span class="input-group-addon"><i class="icon-calender"></i></span> 
                                </div>

                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Kategori <span class="text-danger">*</span></label>
                                <select name="project_id" id="project_id" class="selectpicker form-control" data-style="form-control" required data-error="Project harus dipilih">
                                    <option value="">Pilih Kategori</option>
                                    @foreach ($project as $result)
                                        <option value="{{ $result->project_id }}" {{ $finance->project_id == $result->project_id ? "selected" : "" }}>{{ $result->project }}</option>
                                    @endforeach
                                </select>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Developer <span class="text-danger">*</span></label>
                                <select name="team_id" id="team_id" class="selectpicker form-control" data-style="form-control">
                                    <option value="">Pilih Developer</option>
                                    @foreach ($team as $result)
                                        <option value="{{ $result->team_id }}" {{ $finance->team_id == $result->team_id ? "selected" : "" }}>{{ $result->team_name }} ({{ $result->team_position }})</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Posting Key</label>
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="pm_finance_pkey" value="1" {{ $finance->pm_finance_pkey == 1 ? "checked" : "" }}>
                                            <label>Debet</label>
                                        </div>
                                    </label>
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="pm_finance_pkey" value="0" {{ $finance->pm_finance_pkey == 0 ? "checked" : "" }}>
                                            <label>Kredit</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Debit</label>
                                <input type="text" class="form-control" name="pm_finance_debet" value="{{ $finance->pm_finance_debet }}" placeholder="Masukan nominal debit" required data-error="Keterangan harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label>Kredit</label>
                                <input type="text" class="form-control" name="pm_finance_credit" value="{{ $finance->pm_finance_credit }}" placeholder="Masukan nominal kredit" required data-error="Keterangan harus diisi">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-primary">Simpan</button>
                            <a href="{{ url('pfinance') }}" class="btn btn-danger">Batal</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        });

        var pkey = "{{ $finance->pm_finance_pkey }}";

        if(pkey == 1){
            $('input[name="pm_finance_debet"]').attr('disabled',false);
            $('input[name="pm_finance_credit"]').attr('disabled',true);
        } else {
            $('input[name="pm_finance_debet"]').attr('disabled',true);
            $('input[name="pm_finance_credit"]').attr('disabled',false);
        }

        $('input[name="pm_finance_pkey"]').bind('change', function(){
            if($(this).val() == 1){
                $('input[name="pm_finance_debet"]').attr('disabled',false);
                $('input[name="pm_finance_debet"]').val(0);
                $('input[name="pm_finance_credit"]').attr('disabled',true);
                $('input[name="pm_finance_credit"]').val(0);
            } else {
                $('input[name="pm_finance_debet"]').attr('disabled',true);
                $('input[name="pm_finance_debet"]').val(0);
                $('input[name="pm_finance_credit"]').attr('disabled',false);
                $('input[name="pm_finance_credit"]').val(0);
            }
        });


        $('#project_id').bind('change', function(){
            var id = $(this).val();
            var url = "{{ url('pfinance/team/') }}" +"/"+ id;

            $.get(url, function(data){
                console.log(data);
                $('#team_id').html(data);
                $('#team_id').selectpicker('refresh');
            });
        });
    </script>
    <script src="{{ asset('js/validator.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('eliteadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@endpush