@extends('layouts.root')

@section('title', 'List Keuangan Project')

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Keuangan Project</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Project</a></li>
                    <li class="active">Keuangan Project</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        
        <div class="row">
            <div class="col-md-12">
                
                @if(Session::has('status'))
                    @if(Session::get('status') == '200')
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ Session::get('msg') }}. 
                    </div>
                    @elseif(Session::get('status') == 'err')
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::get('msg') }}
                        </div>
                    @endif
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <a href="{{ url('pfinance/create') }}" class="btn btn-primary">
                            <i class="ti-plus"></i> &nbsp; Tambah Keuangan Project</i>
                        </a>
                        <div class="pull-right">
                            <form class="form-inline float-right" action="{{ url('pfinance') }}">
                                <div class="form-group" style="margin-right: 10px;">
                                    <select name="param" id="param" class="form-control">
                                        <option value="">-- Cari Berdasarkan --</option>
                                        <option value="pm_finance_info" {{ Request::get('param') == "pm_finance_info" ? "selected" : "" }} >Keterangan</option>
                                        <option value="project" {{ Request::get('param') == "project" ? "selected" : "" }} >Project</option>
                                        <option value="pm_finance_date" {{ Request::get('param') == "pm_finance_date" ? "selected" : "" }} >Tanggal</option>
                                    </select>
                                </div>
                                <div class="form-group" style="margin-right: 10px;">
                                    <input type="text" name="key" id="key" class="form-control" value='{{ Request::has("key") ? Request::get("key") : "" }}' placeholder="Masukan pencarian">
                                </div>
                                <button style="margin-right: 10px;" type="submit" class="btn btn-success"><i class="icon-search"></i> Cari</button>
                                <a href="{{ url('pfinance') }}" class="btn btn-default" style="text-decoration:none"><i class="ti-reload"></i></a>
                            </form>
                        </div>
                    </div>
                    <div class="panel-wrapper collapse in" aria-expanded="true">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th width="20%">Keterangan</th>
                                            <th>Project</th>
                                            <th>Developer</th>
                                            <th>Tanggal</th>
                                            <th>Debet</th>
                                            <th>Kredit</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(sizeof($finance) > 0)
                                            @foreach ($finance as $key => $result)
                                                <tr>
                                                    <td>{{ ($finance->currentpage()-1) * $finance->perpage() + $key + 1  }}</td>
                                                    <td>{{ $result->pm_finance_info }}</td>
                                                    <td>{{ $result->getProject->project }}</td>
                                                    <td>{{ isset($result->getTeam) ? $result->getTeam->team_name : "-" }}</td>
                                                    <td>{{ date('d M Y', strtotime($result->pm_finance_date)) }}</td>
                                                    <td>Rp. {{ number_format($result->pm_finance_debet) }}</td>
                                                    <td>Rp. {{ number_format($result->pm_finance_credit) }}</td>
                                                    <td>
                                                        <a href="{{ url('pfinance/edit/'. $result->pm_finance_id) }}" class="btn btn-info">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <button data-toggle="tooltip" title="Hapus" data-action="{{ url('pfinance/delete/'. $result->pm_finance_id) }}" class="btn btn-danger btn-delete">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr> <td colspan="8">Data Keuangan Project Tidak Tersedia. </td> </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            {{ $finance->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .row -->
    </div>
    <!-- /.container-fluid -->

@include('modal_delete');
@endsection

@push('script')
    <script type="text/javascript">
        $('.btn-delete').click(function(){
            var action = $(this).data('action');

            $('#modalDelete').modal('show');
            $('.modal-title').html('Hapus Keuangan Project');

            $('#formDelete').attr('action',action);
            $('#formDelete').append("<input type='hidden' name='_method' value='DELETE'>");
        });
    </script>
@endpush
