@extends('layouts.root')

@section('title', 'Tambah Project')
    

@push('style')    
    <link href="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Project</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li><a href="{{ url('project') }}">Project</a></li>
                    <li class="active">Tambah</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->

        <form class="form-horizontal" data-toggle="validator" method="POST" action="{{ url('project') }}" enctype="multipart/form-data">
            @csrf
            
            <div class="white-box row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Nama Project <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="project" placeholder="Masukan nama project" required data-error="Nama project harus diisi">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Tanggal Mulai<span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="datepicker-autoclose" name="project_start" placeholder="mm/dd/yyyy" required data-error="Tanggal mulai harus dipilih">
                            <span class="input-group-addon"><i class="icon-calender"></i></span> 
                        </div>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Tanggal Selesai<span class="text-danger">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="datepicker-autoclose-2" name="project_end" placeholder="mm/dd/yyyy" required data-error="Tanggal selesai harus dipilih">
                            <span class="input-group-addon"><i class="icon-calender"></i></span> 
                        </div>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Kategori <span class="text-danger">*</span></label>
                        <select name="categories_id" id="categories_id" class="selectpicker form-control" data-style="form-control" required data-error="Kategori harus dipilih">
                            <option value="">Pilih Kategori</option>
                            @foreach ($category as $result)
                                <option value="{{ $result->categories_id }}">{{ $result->category }}</option>
                            @endforeach
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Nominal</label>
                        <input type="text" value="0" class="form-control" name="project_price" placeholder="Masukan nominal debit" required data-error="Keterangan harus diisi">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Sumber Project</label>
                        <textarea name="project_source" id="project_source" cols="30" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Upload Penawaran</label>
                        <input type="file" value="0" class="form-control" name="project_quotation" placeholder="Masukan nominal debit" required data-error="Penawaran harus diisi">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <hr>
                        <button class="btn btn-info btn-clone" type="button">Tambah Tim</button>
                    </div>
                </div>
                <div id="teams" class="col-12">
                    <div class="team_1">
                        <div class="col-6" style="float:left">
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" class="form-control" name="team_name[]">
                            </div>
                        </div>
                        <div class="col-6" style="float:left">
                            <div class="form-group">
                                <label for="">Posisi</label>
                                <select name="team_position[]" class="form-control">
                                    <option value="">Pilih Posisi</option>
                                    <option value="Fullstack Developer">Fullstack Developer</option>
                                    <option value="Backend Developer">Backend Developer</option>
                                    <option value="Frontend Developer">Frontend Developer</option>
                                    <option value="API Developer">API Developer</option>
                                    <option value="Android Developer">Android Developer</option>
                                    <option value="IOS Developer">IOS Developer</option>
                                    <option value="Design UI / UX">Design UI / UX</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <button class="btn btn-primary">Simpan</button>
                        <a href="{{ url('project') }}" class="btn btn-danger">Batal</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script>
        jQuery(document).ready(function() {
            $(".select2").select2();
            $('.selectpicker').selectpicker();

            jQuery('#datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#datepicker-autoclose-2').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        });

        $('.btn-clone').bind('click', function(){
            var cur_num = 1;

            var cloned = $(".team_" + cur_num).clone(true, true).get(0);
            ++cur_num;
            cloned.id = "team_" + cur_num;             // Change the div itself.
            $(cloned).find("input[name='team_name[]']").val("").attr("data-id",cur_num).each(function(index, element) {   // And all inner elements.
                if(element.id)
                {
                    var matches = element.id.match(/(.+)_\d+/);
                    if(matches && matches.length >= 2)            // Captures start at [1].
                        element.id = matches[1] + "_" + cur_num;
                }
            });

            $(cloned).appendTo($("#teams"));
        })
    </script>
    <script src="{{ asset('js/validator.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('eliteadmin/plugins/bower_components/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('eliteadmin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@endpush