<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('login');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

// Routing Group System
Route::group(['namespace' => 'System'], function(){
    
    // Routing Group Level
    Route::group(['prefix' => 'level'], function(){
        Route::get('/', 'LevelController@index');
        Route::post('/', 'LevelController@store');
        Route::get('/create', 'LevelController@create');
        Route::get('/edit/{id}', 'LevelController@edit');
        Route::put('/update/{id}', 'LevelController@update');
        Route::delete('/delete/{id}', 'LevelController@delete');
    });

    // Routing Group User
    Route::group(['prefix' => 'user'], function(){
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@store');
        Route::get('/create', 'UserController@create');
        Route::get('/edit/{id}', 'UserController@edit');
        Route::put('/update/{id}', 'UserController@update');
        Route::delete('/delete/{id}', 'UserController@delete');
    });

    // Routing Group Menu
    Route::group(['prefix' => 'menu'], function(){
        Route::get('/', 'MenuController@index');
        Route::post('/', 'MenuController@store');
        Route::get('/create', 'MenuController@create');
        Route::get('/edit/{id}', 'MenuController@edit');
        Route::put('/update/{id}', 'MenuController@update');
        Route::delete('/delete/{id}', 'MenuController@delete');
    });

    // Routing Group Module
    Route::group(['prefix' => 'module'], function(){
        Route::get('/', 'ModuleController@index');
        Route::post('/', 'ModuleController@store');
        Route::get('/create', 'ModuleController@create');
        Route::get('/edit/{id}', 'ModuleController@edit');
        Route::put('/update/{id}', 'ModuleController@update');
        Route::delete('/delete/{id}', 'ModuleController@delete');
    });

    // Routing Group Task
    Route::group(['prefix' => 'task'], function(){
        Route::get('/', 'TaskController@index');
        Route::post('/', 'TaskController@store');
        Route::get('/create', 'TaskController@create');
        Route::get('/edit/{id}', 'TaskController@edit');
        Route::put('/update/{id}', 'TaskController@update');
        Route::delete('/delete/{id}', 'TaskController@delete');
    });

    // Routing Group Role
    Route::group(['prefix' => 'role'], function(){
        Route::get('/', 'RoleController@index');
        Route::post('/', 'RoleController@store');
        Route::get('/create/{id}', 'RoleController@create');
    });
});

// Routing Group Master
Route::group(['namespace' => 'Master'], function(){
    
    // Routing Group FinanceCategories
    Route::group(['prefix' => 'fcategories'], function(){
        Route::get('/', 'FinanceCategoriesController@index');
        Route::post('/', 'FinanceCategoriesController@store');
        Route::get('/create', 'FinanceCategoriesController@create');
        Route::get('/edit/{id}', 'FinanceCategoriesController@edit');
        Route::put('/update/{id}', 'FinanceCategoriesController@update');
        Route::delete('/delete/{id}', 'FinanceCategoriesController@delete');
    });

    // Routing Group ProjectCategories
    Route::group(['prefix' => 'pcategories'], function(){
        Route::get('/', 'ProjectCategoriesController@index');
        Route::post('/', 'ProjectCategoriesController@store');
        Route::get('/create', 'ProjectCategoriesController@create');
        Route::get('/edit/{id}', 'ProjectCategoriesController@edit');
        Route::put('/update/{id}', 'ProjectCategoriesController@update');
        Route::delete('/delete/{id}', 'ProjectCategoriesController@delete');
    });
});

// Routing Group Finance
Route::group(['namespace' => 'Finance'], function(){
    
    // Routing Group Finance
    Route::group(['prefix' => 'finance'], function(){
        Route::get('/', 'FinanceController@index');
        Route::post('/', 'FinanceController@store');
        Route::get('/create', 'FinanceController@create');
        Route::get('/edit/{id}', 'FinanceController@edit');
        Route::get('/detail/{id}', 'FinanceController@show');
        Route::put('/update/{id}', 'FinanceController@update');
        Route::delete('/delete/{id}', 'FinanceController@delete');
    });

    // Routing Group FinanceRequest
    Route::group(['prefix' => 'frequest'], function(){
        Route::get('/', 'FinanceRequestController@index');
        Route::post('/', 'FinanceRequestController@store');
        Route::get('/create', 'FinanceRequestController@create');
        Route::get('/edit/{id}', 'FinanceRequestController@edit');
        Route::get('/detail/{id}', 'FinanceRequestController@show');
        Route::get('/approval/{id}', 'FinanceRequestController@approval');
        Route::put('/update/{id}', 'FinanceRequestController@update');
        Route::delete('/delete/{id}', 'FinanceRequestController@delete');
    });
});


// Routing Group Finance
Route::group(['namespace' => 'Project'], function(){
    
    // Routing Group Finance
    Route::group(['prefix' => 'project'], function(){
        Route::get('/', 'ProjectController@index');
        Route::post('/', 'ProjectController@store');
        Route::get('/create', 'ProjectController@create');
        Route::get('/edit/{id}', 'ProjectController@edit');
        Route::get('/detail/{id}', 'ProjectController@show');
        Route::put('/update/{id}', 'ProjectController@update');
        Route::get('/done/{id}', 'ProjectController@done');
        Route::delete('/delete/{id}', 'ProjectController@delete');
    });

    // Routing Group FinanceRequest
    Route::group(['prefix' => 'pfinance'], function(){
        Route::get('/', 'ProjectFinanceController@index');
        Route::post('/', 'ProjectFinanceController@store');
        Route::get('/create', 'ProjectFinanceController@create');
        Route::get('/edit/{id}', 'ProjectFinanceController@edit');
        Route::get('/detail/{id}', 'ProjectFinanceController@show');
        Route::get('/team/{id}', 'ProjectFinanceController@getTeam');
        Route::put('/update/{id}', 'ProjectFinanceController@update');
        Route::delete('/delete/{id}', 'ProjectFinanceController@delete');
    });
});

