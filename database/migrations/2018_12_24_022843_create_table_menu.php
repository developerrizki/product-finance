<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_menus', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->integer('module_id')->unsigned()->nullable();
            $table->string('menu', 100);
            $table->string('menu_icon', 30)->nullable();
            $table->string('menu_url', 30);
            $table->integer('menu_is_sub')->comment('0 : non sub, 1 : is sub');
            $table->integer('menu_parent')->nullable();
            $table->integer('menu_position')->comment('0 : Top');
            $table->timestamps();

            $table->foreign('module_id')->references('module_id')->on('sys_modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_menus');
    }
}
