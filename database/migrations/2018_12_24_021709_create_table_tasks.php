<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_tasks', function (Blueprint $table) {
            $table->increments('task_id');
            $table->integer('module_id')->unsigned();
            $table->string('task', 100);
            $table->timestamps();

            $table->foreign('module_id')->references('module_id')->on('sys_modules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_tasks');
    }
}
