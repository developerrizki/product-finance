<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFinance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finances', function (Blueprint $table) {
            $table->increments('finance_id');
            $table->integer('category_id')->unsigned();
            $table->integer('project_id')->unsigned()->nullable();
            $table->datetime('finance_date');
            $table->string('finance_info');
            $table->integer('finance_debet')->default(0);
            $table->integer('finance_credit')->default(0);
            $table->timestamps();

            $table->foreign('category_id')->references('categories_id')->on('finance_categories');
            $table->foreign('project_id')->references('project_id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finances');
    }
}
