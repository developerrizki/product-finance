<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProjectFinance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_finances', function (Blueprint $table) {
            $table->increments('pm_finace_id');
            $table->datetime('pm_finace_date');
            $table->integer('pm_finace_nominal');
            $table->integer('team_id')->unsigned()->nullable()->comment('Developer');
            $table->timestamps();

            $table->foreign('team_id')->references('team_id')->on('project_teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_finances');
    }
}
