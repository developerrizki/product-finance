<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('project_id');
            $table->integer('user_id')->unsigned()->nullable()->comment('PM');
            $table->string('project', 100);
            $table->date('project_start')->nullable();
            $table->date('project_end')->nullable();
            $table->integer('project_price')->nullable();
            $table->integer('project_status')->default(0)->comment('0 : OnGoing, 1: Done, 2: Terminate');
            $table->string('project_source')->nullable();
            $table->text('project_quotation')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
