<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFinance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('finances', function (Blueprint $table) {
            $table->integer('finance_pkey')->default(1)->after('finance_credit')->comment('1 : Debet, 0: Kredit');
            $table->integer('finance_desc')->nullable()->after('finance_info');
        });

        Schema::table('finance_requests', function (Blueprint $table) {
            $table->integer('categories_id')->unsigned()->nullable()->after('request_id');
            $table->integer('request_status')->default(0)->after('request_nominal')->comment('0: pending, 1 : approved, 2 : rejected');
            $table->foreign('categories_id')->references('categories_id')->on('finance_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('finances', function (Blueprint $table) {
            $table->dropColumn('finance_pkey');
        });

        Schema::table('finance_requests', function (Blueprint $table) {
            $table->dropColumn('categories_id');
        });
    }
}
