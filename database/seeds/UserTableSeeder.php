<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user                    = new App\User;
        $user->username          = 'finance';
        $user->email             = 'finance@sampulkreativ.com';
        $user->email_verified_at = now();
        $user->name              = 'Finance Sampulkreativ';
        $user->password          = bcrypt('finance890');
        $user->level_id          = 1;
        $user->save();
    }
}
