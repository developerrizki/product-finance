<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level                    = new App\Models\Level;
        $level->level             = 'Super Admin';
        $level->save();
    }
}
